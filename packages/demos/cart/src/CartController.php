<?php

namespace Demos\Cart;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {
        $product = Product::find($request->id);

        $id = session()->getId();

        if(\Demos\Cart\Cart::where([['product_id', '=', $request->id], ['session_id', '=', $id]])->first())
        {
            return null;
        }


        $cart = new \Demos\Cart\Cart();

        $item = $cart->addToCart($product, $id);

        return $item;

    }

    public function getCart()
    {
        $id = session()->getId();

        $cart = \Demos\Cart\Cart::getCart($id)->with(['product.image'])->get();

        return $cart;
    }
}
