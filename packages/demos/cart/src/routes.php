<?php

Route::group(['middleware' => ['web']], function () {
    Route::post('add-to-cart', 'Demos\Cart\CartController@addToCart');
    Route::get('get-cart', 'Demos\Cart\CartController@getCart');
});
