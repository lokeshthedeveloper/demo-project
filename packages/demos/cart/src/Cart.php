<?php

namespace Demos\Cart;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['session_id', 'quantity', 'price', 'product_id'];

    public function scopeGetCart($query, $id)
    {
        return $query->where('session_id', $id);
    }

    public function addToCart(Product $product, $id)
    {
        $this->session_id = $id;
        $this->fill($product->attributes);
        $this->product_id = $product->id;

        $this->save();

        return Cart::with('product.image')->find($this->id);

    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
