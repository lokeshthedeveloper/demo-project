<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class FackProduct extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = \Faker\Factory::create();

       for ($i = 0; $i < 20; $i++)
       {

           $image = $faker->image();
           $imageFile = new \Illuminate\Http\File($image);

           if($extn = pathinfo($imageFile, PATHINFO_EXTENSION))
           {
               $product = \App\Models\Product::create([
                   'name' => $faker->name,
                   'quantity' => $faker->randomDigit,
                   'price' => $faker->randomNumber(2)
               ]);

               $product->images()->create(
                   [
                       'name' => Storage::disk('public')->putFile(\App\Models\ProductImage::IMAGE_PATH, $imageFile)
                   ]
               );
           }

       }
    }
}
