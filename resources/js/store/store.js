import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        cart: []
    },
    actions: {
        SET_CART(context, cart) {
            context.commit('UPDATE_CART', cart)
        },
        FETCH_CART(context) {
            const url = '/get-cart'
            axios.get(url, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then((response) => {
                if (response.data) {
                    context.commit('PUSH_CART', response.data)
                }
            })
        }
    },
    mutations: {
        UPDATE_CART: (state, cart) => {
            state.cart.push({
                id: cart.id,
                price: cart.price,
                product_id: cart.product_id,
                quantity: cart.quantity,
                session_id: cart.session_id,
                updated_at: cart.updated_at,
                product: cart.product
            })
        },
        PUSH_CART: (state, carts) => {
            carts.map((cart) => {
                state.cart.push(cart)
            })
        }
    },
    getters: {
        GET_CART: state => {
            return state.cart
        }
    }
})