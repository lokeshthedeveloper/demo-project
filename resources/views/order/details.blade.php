@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Fill Order Detail</div>
                    <div class="card-body">
                        {!! Form::open(['method' => 'post', 'route' => 'order']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::customText('Name', 'name', old('name'), $errors->first('name')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::customText('Email', 'email', old('email'), $errors->first('email')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::customText('Contact Number', 'contact_number', old('contact_number'), $errors->first('contact_number')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::customText('City', 'city', old('city'), $errors->first('city')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::customText('State', 'state', old('state'), $errors->first('state')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::customText('Country', 'country', old('country'), $errors->first('country')) !!}
                            </div>
                            <div class="col-md-12">
                                {!! Form::customTextArea('Address', 'address', old('address'), $errors->first('address'), ['rows' => 2]) !!}
                            </div>
                            <div class="col-md-12">
                                <input type="submit" name="submit" value="Place Order by COD" class="btn btn-primary">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
