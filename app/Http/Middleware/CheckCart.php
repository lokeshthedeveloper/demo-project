<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Demos\Cart\Cart;

class CheckCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session_id = Session::getId();

        $cart = Cart::getCart($session_id)->get();

        if(!$cart->count())
        {
            return redirect('/')->with([
                'alert' => [
                    'type' => true,
                    'msg' => 'Your cart is empty.'
                ]]);
        }


        return $next($request);
    }
}
