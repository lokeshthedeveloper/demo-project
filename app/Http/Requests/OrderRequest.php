<?php

namespace App\Http\Requests;

use App\Rules\ValidContactNumber;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:70',
            'email' => 'required|email',
            'contact_number' => [ 'required', new ValidContactNumber ],
            'city' => 'required|max:50',
            'state' => 'required:max:50',
            'country' => 'required|max:50',
            'address' => 'required|max:250'
        ];
    }
}
