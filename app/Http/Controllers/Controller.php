<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    const MESSAGE_SUCCESS = 1;
    const MESSAGE_ERROR = 2;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function setMessage($message, $type)
    {
        return [
            'alert' => [
                'type' => $type,
                'msg' => $message
            ]
        ];
    }
}
