<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Rules\ValidContactNumber;
use Demos\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function __invoke(OrderRequest $request)
    {

        try{
            $session_id = Session::getId();

            $cart = Cart::getCart($session_id)->get();

            if (!$cart->count()) {
                return redirect()->back()->with($this->setMessage('Your cart is empty', self::MESSAGE_ERROR));
            }

            \DB::beginTransaction();

            // Create Order
            $order = self::createOrder($cart, $request);

            // Create Order Details
            self::createOrderDetails($order, $request);

            //Crate order items
            self::createItem($order, $cart);

            \DB::commit();

            return redirect('/')->with($this->setMessage('Your order successfully done.', self::MESSAGE_SUCCESS));

        }catch (\Exception $e)
        {
            return redirect()->back()->with($this->setMessage($e->getMessage(), self::MESSAGE_ERROR));
        }
    }


    /*
    * Create order
     * @param  \Illuminate\Http\Request  $request
     * @param  Demos\Cart\Cart  $cart
     * @return \App\Models\Order $order
     * */

    protected function createOrder($cart, $request): Order
    {
        $order = new Order();

        $order->total = $cart->sum('price');

        $order->user()->associate($request->user());

        $order->save();

        return $order;
    }

    /*
    * Create order details
    * @param  \Illuminate\Http\Request  $request
     * @param \App\Models\Order $order
    * */

    protected function createOrderDetails($order, $request)
    {
      $order->detail()->create($request->all());
    }

    /*
   * Create order items
   * @param  Demos\Cart\Cart  $cart
    * @param \App\Models\Order $order
   * */

    protected function createItem($order, $cart)
    {
        foreach ($cart as $item) {
            $order->items()->create(
                [
                    'price' => $item->price,
                    'quantity' => $item->quantity,
                    'product_id' => $item->product_id
                ]
            );

            $item->delete();
        }
    }



    /*
     * Show order details form
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function orderDetails(Request $request)
    {
        $user = $request->user();

        return view('order.details', compact('user'));
    }

}
