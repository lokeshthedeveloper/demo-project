<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $products;

    public function __construct()
    {
        $this->products = Product::active()->with('image');
    }

    public function __invoke()
    {
        $products = $this->products->get();

        return $products;
    }
}
