<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Form::macro('customText', function ($label, $name, $value, $error, $options = []) {

            ob_start();

            ?>
            <div class="form-group <?php echo $error ? ' error' : '' ?>">
                <?php echo \Form::label($name, $label) ?>
                <?php echo \Form::text($name, $value, array_merge(['class' => 'form-control'], $options)) ?>
            </div>

            <?php
            echo !empty($error) ? '<label for="' . $name . '" class="error">' . $error . '</label>' : '';

            return ob_get_clean();
        });


        \Form::macro('customTextArea', function ($label, $name, $value, $error, $options = []) {

            ob_start();

            ?>
            <div class="form-group <?php echo $error ? ' error' : '' ?>">
                <?php echo \Form::label($name, $label) ?>
                <?php echo \Form::textarea($name, $value, array_merge(['class' => 'form-control'], $options)) ?>
            </div>

            <?php
            echo !empty($error) ? '<label for="' . $name . '" class="error">' . $error . '</label>' : '';

            return ob_get_clean();
        });
    }
}
