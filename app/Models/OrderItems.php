<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $fillable = ['price', 'quantity', 'product_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
