<?php

namespace App\Models;

use App\Facades\ImageClass;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    const IMAGE_PATH = 'product-images';

    protected $appends = [ 'image_path' ];

    protected $fillable = [
        'name'
    ];

    public function getImagePathAttribute()
    {

        return ImageClass::getProductImage(storage_path('app/public/' . $this->name), asset('storage/' . $this->name));

        return ($this->hasImage() && filesize(storage_path('app/public/' . $this->name))) ? asset('storage/' . $this->name) : asset('default.jpg');
    }

    public function hasImage()
    {
        return (bool) $this->name && file_exists(storage_path('app/public/' . $this->name));
    }

    public function getImage()
    {
        return public_path($this->name);
    }
}
