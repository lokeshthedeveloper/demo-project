<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{

    protected $fillable = [
        'name',
        'email',
        'contact_number',
        'city',
        'state',
        'country',
        'address'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
