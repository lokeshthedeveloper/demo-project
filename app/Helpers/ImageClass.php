<?php


namespace App\Helpers;

use File;


class ImageClass
{
    /*
     * Helper to check file exists or not and return the path
     * */

    public function getProductImage($orig, $path)
    {
        if (file_exists($orig) && filesize($orig))
        {

            return $path;

        }
        return asset('default.jpg');
    }

}